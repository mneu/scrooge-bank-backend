import {MigrationInterface, QueryRunner} from "typeorm";

export class InitialCreate1616730515404 implements MigrationInterface {
    name = 'InitialCreate1616730515404'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "user_types" ("id" smallint NOT NULL, "name" character varying(300) NOT NULL, CONSTRAINT "PK_3f05efd7b52a7eca1f6b6f75e45" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "operation_types" ("id" smallint NOT NULL, "name" character varying(300) NOT NULL, CONSTRAINT "PK_271e0ed5b59d103f770514c4895" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "operations" ("id" SERIAL NOT NULL, "customerId" integer NOT NULL, "createdByUserId" integer NOT NULL, "amount" numeric(18,2) NOT NULL, "operationTypeId" smallint NOT NULL, "createdAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_7b62d84d6f9912b975987165856" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_31a253d23ae01a8868cef3a50f" ON "operations" ("customerId") `);
        await queryRunner.query(`CREATE INDEX "IDX_487142abf43ecd5fae71258647" ON "operations" ("createdByUserId") `);
        await queryRunner.query(`CREATE INDEX "IDX_126032075a44a02adda3322bfc" ON "operations" ("operationTypeId") `);
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "bankId" integer, "customerId" integer, "userTypeId" smallint NOT NULL, "firstName" character varying(400), "lastName" character varying(400), "email" character varying(500) NOT NULL, "password" character varying(300) NOT NULL, "mailConfirmed" boolean NOT NULL, "active" boolean NOT NULL, "createdAt" TIMESTAMP NOT NULL, CONSTRAINT "REL_c6c520dfb9a4d6dd749e73b13d" UNIQUE ("customerId"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_76a4450ed7980762ba457b98f7" ON "users" ("bankId") `);
        await queryRunner.query(`CREATE INDEX "IDX_c6c520dfb9a4d6dd749e73b13d" ON "users" ("customerId") `);
        await queryRunner.query(`CREATE INDEX "IDX_a8f61a419ce5313def9b0f4c21" ON "users" ("userTypeId") `);
        await queryRunner.query(`CREATE INDEX "IDX_97672ac88f789774dd47f7c8be" ON "users" ("email") `);
        await queryRunner.query(`CREATE TABLE "loan_payments" ("id" SERIAL NOT NULL, "loanId" integer NOT NULL, "amount" numeric(18,2) NOT NULL, "createdAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_db75e38243b5f2cb9e728da4d0f" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_c0268488cc84d23c1e5c6c1910" ON "loan_payments" ("loanId") `);
        await queryRunner.query(`CREATE TABLE "loans" ("id" SERIAL NOT NULL, "customerId" integer NOT NULL, "createdByUserId" integer NOT NULL, "amount" numeric(18,2) NOT NULL, "paid" numeric(18,2) NOT NULL, "finalized" boolean NOT NULL, "createdAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_5c6942c1e13e4de135c5203ee61" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d7002fbfdf6379b25a869d40e1" ON "loans" ("customerId") `);
        await queryRunner.query(`CREATE INDEX "IDX_4df5f0f9a36b6fc330afe1bc10" ON "loans" ("createdByUserId") `);
        await queryRunner.query(`CREATE INDEX "IDX_7b99e2dc195d48df3a68d5b240" ON "loans" ("finalized") `);
        await queryRunner.query(`CREATE TABLE "customers" ("id" SERIAL NOT NULL, "bankId" integer NOT NULL, "userId" integer NOT NULL, "document" character varying(200) NOT NULL, "firstName" character varying(1500) NOT NULL, "lastName" character varying(1500) NOT NULL, "address" character varying(1500) NOT NULL, "email" character varying(500) NOT NULL, "mailConfirmed" boolean NOT NULL, "accountMoney" numeric(18,2) NOT NULL, "active" boolean NOT NULL, "createdAt" TIMESTAMP NOT NULL, CONSTRAINT "REL_b8512aa9cef03d90ed5744c94d" UNIQUE ("userId"), CONSTRAINT "PK_133ec679a801fab5e070f73d3ea" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_d87eef3e0ef3dd533123e095bb" ON "customers" ("bankId") `);
        await queryRunner.query(`CREATE INDEX "IDX_b8512aa9cef03d90ed5744c94d" ON "customers" ("userId") `);
        await queryRunner.query(`CREATE INDEX "IDX_68c9c024a07c49ad6a2072d23c" ON "customers" ("document") `);
        await queryRunner.query(`CREATE INDEX "IDX_d4a7694ae97c32d3d286771229" ON "customers" ("firstName") `);
        await queryRunner.query(`CREATE INDEX "IDX_8e11140e3639e6d35a9f79f980" ON "customers" ("lastName") `);
        await queryRunner.query(`CREATE INDEX "IDX_cb0313acf54b486381e2f0c6ad" ON "customers" ("address") `);
        await queryRunner.query(`CREATE INDEX "IDX_8536b8b85c06969f84f0c098b0" ON "customers" ("email") `);
        await queryRunner.query(`CREATE TABLE "banks" ("id" SERIAL NOT NULL, "name" character varying(500) NOT NULL, "address" character varying(1500) NOT NULL, "maxLoanMoney" numeric(18,2) NOT NULL, "customersMoney" numeric(18,2) NOT NULL, "totalMoney" numeric(18,2) NOT NULL, "createdAt" TIMESTAMP NOT NULL, CONSTRAINT "PK_3975b5f684ec241e3901db62d77" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_bc680de8ba9d7878fddcecd610" ON "banks" ("name") `);
        await queryRunner.query(`ALTER TABLE "operations" ADD CONSTRAINT "FK_31a253d23ae01a8868cef3a50f4" FOREIGN KEY ("customerId") REFERENCES "customers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "operations" ADD CONSTRAINT "FK_487142abf43ecd5fae712586474" FOREIGN KEY ("createdByUserId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "operations" ADD CONSTRAINT "FK_126032075a44a02adda3322bfcd" FOREIGN KEY ("operationTypeId") REFERENCES "operation_types"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_76a4450ed7980762ba457b98f77" FOREIGN KEY ("bankId") REFERENCES "banks"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_a8f61a419ce5313def9b0f4c21e" FOREIGN KEY ("userTypeId") REFERENCES "user_types"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "users" ADD CONSTRAINT "FK_c6c520dfb9a4d6dd749e73b13de" FOREIGN KEY ("customerId") REFERENCES "customers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "loan_payments" ADD CONSTRAINT "FK_c0268488cc84d23c1e5c6c1910d" FOREIGN KEY ("loanId") REFERENCES "loans"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "loans" ADD CONSTRAINT "FK_d7002fbfdf6379b25a869d40e12" FOREIGN KEY ("customerId") REFERENCES "customers"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "loans" ADD CONSTRAINT "FK_4df5f0f9a36b6fc330afe1bc101" FOREIGN KEY ("createdByUserId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "customers" ADD CONSTRAINT "FK_d87eef3e0ef3dd533123e095bb0" FOREIGN KEY ("bankId") REFERENCES "banks"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "customers" ADD CONSTRAINT "FK_b8512aa9cef03d90ed5744c94d7" FOREIGN KEY ("userId") REFERENCES "users"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "customers" DROP CONSTRAINT "FK_b8512aa9cef03d90ed5744c94d7"`);
        await queryRunner.query(`ALTER TABLE "customers" DROP CONSTRAINT "FK_d87eef3e0ef3dd533123e095bb0"`);
        await queryRunner.query(`ALTER TABLE "loans" DROP CONSTRAINT "FK_4df5f0f9a36b6fc330afe1bc101"`);
        await queryRunner.query(`ALTER TABLE "loans" DROP CONSTRAINT "FK_d7002fbfdf6379b25a869d40e12"`);
        await queryRunner.query(`ALTER TABLE "loan_payments" DROP CONSTRAINT "FK_c0268488cc84d23c1e5c6c1910d"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_c6c520dfb9a4d6dd749e73b13de"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_a8f61a419ce5313def9b0f4c21e"`);
        await queryRunner.query(`ALTER TABLE "users" DROP CONSTRAINT "FK_76a4450ed7980762ba457b98f77"`);
        await queryRunner.query(`ALTER TABLE "operations" DROP CONSTRAINT "FK_126032075a44a02adda3322bfcd"`);
        await queryRunner.query(`ALTER TABLE "operations" DROP CONSTRAINT "FK_487142abf43ecd5fae712586474"`);
        await queryRunner.query(`ALTER TABLE "operations" DROP CONSTRAINT "FK_31a253d23ae01a8868cef3a50f4"`);
        await queryRunner.query(`DROP INDEX "IDX_bc680de8ba9d7878fddcecd610"`);
        await queryRunner.query(`DROP TABLE "banks"`);
        await queryRunner.query(`DROP INDEX "IDX_8536b8b85c06969f84f0c098b0"`);
        await queryRunner.query(`DROP INDEX "IDX_cb0313acf54b486381e2f0c6ad"`);
        await queryRunner.query(`DROP INDEX "IDX_8e11140e3639e6d35a9f79f980"`);
        await queryRunner.query(`DROP INDEX "IDX_d4a7694ae97c32d3d286771229"`);
        await queryRunner.query(`DROP INDEX "IDX_68c9c024a07c49ad6a2072d23c"`);
        await queryRunner.query(`DROP INDEX "IDX_b8512aa9cef03d90ed5744c94d"`);
        await queryRunner.query(`DROP INDEX "IDX_d87eef3e0ef3dd533123e095bb"`);
        await queryRunner.query(`DROP TABLE "customers"`);
        await queryRunner.query(`DROP INDEX "IDX_7b99e2dc195d48df3a68d5b240"`);
        await queryRunner.query(`DROP INDEX "IDX_4df5f0f9a36b6fc330afe1bc10"`);
        await queryRunner.query(`DROP INDEX "IDX_d7002fbfdf6379b25a869d40e1"`);
        await queryRunner.query(`DROP TABLE "loans"`);
        await queryRunner.query(`DROP INDEX "IDX_c0268488cc84d23c1e5c6c1910"`);
        await queryRunner.query(`DROP TABLE "loan_payments"`);
        await queryRunner.query(`DROP INDEX "IDX_97672ac88f789774dd47f7c8be"`);
        await queryRunner.query(`DROP INDEX "IDX_a8f61a419ce5313def9b0f4c21"`);
        await queryRunner.query(`DROP INDEX "IDX_c6c520dfb9a4d6dd749e73b13d"`);
        await queryRunner.query(`DROP INDEX "IDX_76a4450ed7980762ba457b98f7"`);
        await queryRunner.query(`DROP TABLE "users"`);
        await queryRunner.query(`DROP INDEX "IDX_126032075a44a02adda3322bfc"`);
        await queryRunner.query(`DROP INDEX "IDX_487142abf43ecd5fae71258647"`);
        await queryRunner.query(`DROP INDEX "IDX_31a253d23ae01a8868cef3a50f"`);
        await queryRunner.query(`DROP TABLE "operations"`);
        await queryRunner.query(`DROP TABLE "operation_types"`);
        await queryRunner.query(`DROP TABLE "user_types"`);
    }

}
