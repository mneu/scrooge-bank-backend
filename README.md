# Scrooge Bank: NestJS API Demo Project
<p>
Scrooge Bank has opened for business! We are targeting customers who are technically savvy and have decided they love doing banking all via API Requests.
</p>

# Technology used for Scrooge Bank API Backend
<ul>
    <li>NestJS with TypeScript</li>
    <li>Fastify Adapter for extreme performance</li>
    <li>Fastify Helmet & csrf for extra security</li>
    <li>Node v14 LTS</li>
    <li>NGINX as reverse proxy</li>
    <li>Debian Linux as backend server</li>
    <li>Let's Encrypt to generate the production TLS certificates</li>
    <li>PotgreSQL v12</li>
    <li>TypeORM</li>
    <li>Jest (testing framework)</li>
</ul>

# Own User Stories
### Customer account balance
Why did you decide to pick that story?<br/>
Because an user maybe want to know his current account balance<br/>
<br/>
What is the user value this story delivers?<br/>
Get usefull information abaut his current account status<br/>

### Customer loan status
Why did you decide to pick that story?<br/>
What is the user value this story delivers?<br/>
Answer to both questions is tha same: A customer needs to be informed of when he owes from his loan<br/>

### Loans given between range of dates 
Why did you decide to pick that story?<br/>
What is the user value this story delivers?<br/>
Answer to both questions is tha same: Is an interesting dashboard indicator for the bank<br/>

### Average money borrowed by number of customers
Why did you decide to pick that story?<br/>
What is the user value this story delivers?<br/>
Answer to both questions is tha same: Is an interesting dashboard indicator for the bank<br/>

### Average money by number of customers
Why did you decide to pick that story?<br/>
What is the user value this story delivers?<br/>
Answer to both questions is tha same: Is an interesting dashboard indicator for the bank<br/>

# Database Schema

![Kiku](database_schema.png)

# How to deploy

<ul>
    <li>Clone this project into a folder of your choice</li>
    <li>Modify config.env with your environment values</li>
    <li>Run the command "npm run build-debug" to build the project and copy assets to publish folder</li>
    <li>Run the command "npm run seed" to create the database structure and seed basic data</li>
    <li>Run the command "npm run start" to put online the API server</li>
    <li>Enjoy Scrooge Bank API!!!</li>
</ul>

## Test
To test the project you can use the command "npm run test".

## Potman collection
You can test this project with attached [Postman](https://www.postman.com/) collection, using the 7337 port.<br/>
[Here](postman_collection/) you can download the collection.<br/>
<br/>
### Use this users:

#### Bank Admin
<ul>
    <li>User: scrooge@gmail.com</li>
    <li>Password: password123</li>
</ul>

#### Customer
<ul>
    <li>User: bob.cratchit@gmail.com</li>
    <li>Password: christmasghosts</li>
</ul>
<br/>
If you need more users, feel free to create as many as you want.

## NestJS Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

