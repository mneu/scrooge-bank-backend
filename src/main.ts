require('dotenv').config({path: 'config.env'});

import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
const helmet = require('fastify-helmet');
import fastifyCsrf from 'fastify-csrf';
import {
    FastifyAdapter,
    NestFastifyApplication,
} from '@nestjs/platform-fastify';
import {ValidationPipe} from "@nestjs/common";

async function bootstrap() {
    let app: NestFastifyApplication;
    let fastifyAdapter: FastifyAdapter = new FastifyAdapter();

    process.env.be_version = "V:1.0.3-" + process.env.ENVIRONMENT;

    app = await NestFactory.create<NestFastifyApplication>(
        AppModule,
        fastifyAdapter,
    );

    await app.register(helmet);

    await app.register(require('fastify-cookie'), {
        secret: process.env.COOKIES_SECRET, // for cookies signature
        parseOptions: {}     // options for parsing cookies
    })

    await app.register(fastifyCsrf);
    app.enableCors();
    app.useGlobalPipes(new ValidationPipe());
    await app.listen(parseInt(process.env.APP_PORT, 10), '127.0.0.1');
    console.log("Scrooge API Backend backend listen on http(s)://127.0.0.1:" + process.env.APP_PORT);
    console.log("Version: " + process.env.be_version);
}

bootstrap();
