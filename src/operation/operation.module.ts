import { Module } from '@nestjs/common';
import { OperationService } from './operation.service';
import { OperationController } from './operation.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../user/entities/user.entity";
import {Customer} from "../customer/entities/customer.entity";
import {Bank} from "../bank/entities/bank.entity";
import {Operation} from "./entities/operation.entity";
import {AuthModule} from "../auth/auth.module";
import {CustomerModule} from "../customer/customer.module";

@Module({
  imports: [TypeOrmModule.forFeature([User, Customer, Bank, Operation]), AuthModule, CustomerModule],
  providers: [OperationService],
  controllers: [OperationController]
})
export class OperationModule {}
