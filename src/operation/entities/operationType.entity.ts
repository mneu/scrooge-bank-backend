import {Column, Entity, ManyToOne, OneToMany, PrimaryColumn} from "typeorm";
import {Operation} from "./operation.entity";

@Entity({name: 'operation_types'})
export class OperationType {
    constructor(id: number = 0, nombre: string = "") {
        this.id = id;
        this.name = nombre;
    }

    @PrimaryColumn('smallint')
    id: number;

    @Column({length: 300, nullable: false})
    name: string;

    @OneToMany(type => Operation, operation => operation.operationTypeId)
    operations: Operation[];
}