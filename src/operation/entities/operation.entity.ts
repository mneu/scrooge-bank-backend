import {
    Column,
    Entity,
    Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";
import {Customer} from "../../customer/entities/customer.entity";
import {User} from "../../user/entities/user.entity";
import {OperationType} from "./operationType.entity";

@Entity({name: 'operations'})
export class Operation {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    @Index()
    customerId: number;

    @Column({nullable: false})
    @Index()
    createdByUserId: number; // Maybe created by customer user or maybe by bank admin user

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    amount: number;

    @Column({type: 'smallint', nullable: false})
    @Index()
    operationTypeId: number;

    @Column({type: "timestamp without time zone", nullable: false})
    createdAt: Date;
    
    @ManyToOne(type => Customer, customer => customer.operations)
    @JoinColumn({ name: 'customerId' })
    customer: Customer;

    @ManyToOne(type => User, user => user.operationsCreated)
    @JoinColumn({ name: 'createdByUserId' })
    createdByUser: User;

    @ManyToOne(type => OperationType, operationType => operationType.operations)
    @JoinColumn({ name: 'operationTypeId' })
    operationType: OperationType;
}