import {IsNotEmpty, IsNumber} from "class-validator";

export class OperationDto {
    @IsNotEmpty({message: 'You must define customerId'})
    @IsNumber({},{message: 'customerId must be a number'})
    customerId: number;

    @IsNumber({},{message: 'amount must be a number'})
    amount: number;
}