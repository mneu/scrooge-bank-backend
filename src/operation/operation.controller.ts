import {Body, ConflictException, Controller, ForbiddenException, Get, Post, Req, UseGuards} from '@nestjs/common';
import {OperationService} from "./operation.service";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";
import {UserActiveGuard} from "../auth/guards/userActiveGuard.guard";
import {OperationDto} from "./dto/operation.dto";
import {UserTypeEnum} from "../user/enums/userTypeEnum";
import {CustomerService} from "../customer/customer.service";

@Controller('operation')
export class OperationController {
    constructor(private readonly operationService: OperationService, private readonly customerService: CustomerService) {
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Post('witdraw')
    async witdraw(@Req() request, @Body() operationDto: OperationDto) {
        let customerUserId = await this.customerService.getUserIdByCustomer(operationDto.customerId);

        if (!customerUserId)
            throw new ConflictException({error: "customerId not found"});

        if (request.jwtToken.roles.includes(UserTypeEnum.Customer) && customerUserId != request.jwtToken.sub)
            throw new ForbiddenException({error: "As a customer you can't witdraw with another customerId"});

        return await this.operationService.witdraw(operationDto.customerId, request.jwtToken.sub, operationDto.amount);
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Post('deposit')
    async deposit(@Req() request, @Body() operationDto: OperationDto) {
        let customerUserId = await this.customerService.getUserIdByCustomer(operationDto.customerId);

        if (!customerUserId)
            throw new ConflictException({error: "customerId not found"});

        if (request.jwtToken.roles.includes(UserTypeEnum.Customer) && customerUserId != request.jwtToken.sub)
            throw new ForbiddenException({error: "As a customer you can't deposit with another customerId"});

        return await this.operationService.deposit(operationDto.customerId, request.jwtToken.sub, operationDto.amount);
    }
}
