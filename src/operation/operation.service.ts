import {ConflictException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Bank} from "../bank/entities/bank.entity";
import {User} from "../user/entities/user.entity";
import {Operation} from "./entities/operation.entity";
import {Customer} from "../customer/entities/customer.entity";
import {OperationTypeEnum} from "./enums/operationTypeEnum";
import {getManager, Repository} from "typeorm";

@Injectable()
export class OperationService {
    constructor(
        @InjectRepository(Bank) private bankRepository: Repository<Bank>,
        @InjectRepository(User) private userRepository: Repository<User>,
        @InjectRepository(Customer) private customerRepository: Repository<Customer>,
        @InjectRepository(Operation) private operationRepository: Repository<Operation>,
    ) {
    }
    
    async witdraw(customerId: number, userId: number, amount: number) {
        await getManager().transaction(async transactionalEntityManager => {
            let customer: Customer = await this.customerRepository.findOne(customerId);

            if (!customer)
                throw new ConflictException({ error: "customerId not found"});

            if (!customer.active)
                throw new ConflictException({ error: "This is a closed account"});

            if (Number(customer.accountMoney) < amount)
                throw new ConflictException({ error: `You haven't money available in your account. Your balance is $${customer.accountMoney}`});
            
            customer.accountMoney = Number(customer.accountMoney) - amount;

            let bank: Bank = await this.bankRepository.findOne(customer.bankId);
            bank.totalMoney = Number(bank.totalMoney) - amount;
            bank.customersMoney = Number(bank.customersMoney) - amount;

            let operation: Operation = new Operation();
            operation.operationTypeId = OperationTypeEnum.Withdraw;
            operation.amount = amount;
            operation.createdByUserId = userId;
            operation.customerId = customerId;
            operation.createdAt = new Date();

            await transactionalEntityManager.save(customer);
            await transactionalEntityManager.save(bank);
            await transactionalEntityManager.save(operation);
        });
    }

    async deposit(customerId: number, userId: number, amount: number) {
        await getManager().transaction(async transactionalEntityManager => {
            let customer: Customer = await this.customerRepository.findOne(customerId);

            if (!customer)
                throw new ConflictException({ error: "customerId not found"});

            if (!customer.active)
                throw new ConflictException({ error: "This is a closed account"});
            
            customer.accountMoney = Number(customer.accountMoney) + amount;

            let bank: Bank = await this.bankRepository.findOne(customer.bankId);
            bank.totalMoney = Number(bank.totalMoney) + amount;
            bank.customersMoney = Number(bank.customersMoney) + amount;

            let operation: Operation = new Operation();
            operation.operationTypeId = OperationTypeEnum.Deposit;
            operation.amount = amount;
            operation.createdByUserId = userId;
            operation.customerId = customerId;
            operation.createdAt = new Date();

            await transactionalEntityManager.save(customer);
            await transactionalEntityManager.save(bank);
            await transactionalEntityManager.save(operation);
        });
    }
}

