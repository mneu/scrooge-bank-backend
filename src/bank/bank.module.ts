import {forwardRef, Module} from '@nestjs/common';
import { BankService } from './bank.service';
import { BankController } from './bank.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../user/entities/user.entity";
import {UserModule} from "../user/user.module";
import {Bank} from "./entities/bank.entity";
import {AuthModule} from "../auth/auth.module";

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Bank]), UserModule, AuthModule],
  providers: [BankService],
  controllers: [BankController]
})
export class BankModule {}
