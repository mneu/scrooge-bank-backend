import {
    Column,
    Entity,
    Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";
import {Customer} from "../../customer/entities/customer.entity";
import {User} from "../../user/entities/user.entity";

@Entity({name: 'banks'})
export class Bank {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 500, nullable: false})
    @Index()
    name: string;

    @Column({length: 1500, nullable: false})
    address: string;

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    maxLoanMoney: number;

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    customersMoney: number;

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    totalMoney: number;

    @Column({type: "timestamp without time zone", nullable: false})
    createdAt: Date;

    @OneToMany(type => Customer, customer => customer.bankId)
    customers: Customer[];

    @OneToMany(type => User, user => user.bankId)
    users: Customer[];
}