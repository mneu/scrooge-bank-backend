import {Controller, Get, Req, UseGuards} from '@nestjs/common';
import {BankService} from "./bank.service";
import {UserActiveGuard} from "../auth/guards/userActiveGuard.guard";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";
import {UserTypeEnum} from "../user/enums/userTypeEnum";
import {Roles} from "../auth/decorators/roles.decorator";

@Controller('bank')
export class BankController {
    constructor(private readonly bankService: BankService) {}
    
    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Roles(UserTypeEnum.BankAdmin, UserTypeEnum.BankUser)
    @Get('getTotalMoney')
    async getTotalMoney(@Req() request) {
        return await this.bankService.getTotalMoney(request.jwtToken.sub);
    }
}
