import { Injectable } from '@nestjs/common';
import {Connection, Repository} from "typeorm";
import {Bank} from "./entities/bank.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {User} from "../user/entities/user.entity";

@Injectable()
export class BankService {
    constructor(
        @InjectRepository(Bank) private bankRepository: Repository<Bank>,
        @InjectRepository(User) private userRepository: Repository<User>,
        private connection: Connection,
    ) {
    }
    
    private async getUserBankId(userId: number): Promise<number> {
        let userPartial: any = await this.userRepository
            .createQueryBuilder('u')
            .select(['u.bankId as bankid'])
            .where({ 'id': userId })
            .getRawOne();

        return userPartial.bankid;
    }
    
    async getTotalMoney(userId: number): Promise<number> {
        let bakPartial: any = await this.bankRepository
            .createQueryBuilder('b')
            .select(['b.totalMoney as "totalMoney"'])
            .where({ 'id': await this.getUserBankId(userId) })
            .getRawOne();

        bakPartial.totalMoney = Number(bakPartial.totalMoney);
        
        return bakPartial;
    }
}
