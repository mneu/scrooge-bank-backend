require('dotenv').config({path: 'config.env'});
import {ConnectionOptions} from 'typeorm';
import {ConfigModule} from "@nestjs/config";

ConfigModule.forRoot();

const config: ConnectionOptions = {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    entities: [__dirname + '/**/*.entity{.ts,.js}', 'dist/**/*.entity{ .ts,.js}'],
    synchronize: false,
    migrationsTableName: '_migrations',
    migrationsRun: false,
    logging: process.env.DB_LOG_TERM.toLowerCase() == 'true',
    migrations: [__dirname + '/../migrations/**/*{.ts,.js}'],
    cli: {
        migrationsDir: './migrations',
    }
}

export = config;
