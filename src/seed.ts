import {NestFactory} from "@nestjs/core";
import {DatabaseModule} from "./database/database.module";
import {DatabaseService} from "./database/database.service";

async function bootstrap() {
    NestFactory.createApplicationContext(DatabaseModule)
        .then(appContext => {
            const databaseService = appContext.get(DatabaseService)
                .seed()
                .then(() => {
                    console.info('Seed done!');
                })
                .catch(error => {
                    console.error('Seed error!');          
                    throw error;
                })
                .finally(() => process.exit(0));
        })
        .catch(error => {
            throw error;
        });
}
bootstrap();