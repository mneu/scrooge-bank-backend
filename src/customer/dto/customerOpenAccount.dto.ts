import {IsEmail, IsNotEmpty, IsNumber, IsString, MaxLength} from "class-validator";

export class CustomerOpenAccountDto {
    @IsNotEmpty({message: 'You must define bankId'})
    @IsNumber({},{message: 'bankId must be a number'})
    bankId: number;
    
    @IsNotEmpty({message: 'You must define document'})
    @MaxLength(200, {message: 'Max length for document is 200 characters'})
    @IsString({message: 'document must be string'})
    document: string;

    @IsNotEmpty({message: 'You must define firstName'})
    @MaxLength(1500, {message: 'Max length for firstName is 1500 characters'})
    @IsString({message: 'firstName must be string'})
    firstName: string;

    @IsNotEmpty({message: 'You must define lastName'})
    @MaxLength(1500, {message: 'Max length for lastName is 1500 characters'})
    @IsString({message: 'lastName must be string'})
    lastName: string;

    @IsNotEmpty({message: 'You must define address'})
    @MaxLength(1500, {message: 'Max length for address is 1500 characters'})
    @IsString({message: 'address must be string'})
    address: string;

    @IsNotEmpty({message: 'You must define email'})
    @MaxLength(500, {message: 'Max length for email is 500 characters'})
    @IsString({message: 'email must be string'})
    @IsEmail({},{message: 'email must be a valid email address'})
    email: string;

    @IsNotEmpty({message: 'You must define password'})
    @MaxLength(150, {message: 'Max length for password is 150 characters'})
    @IsString({message: 'password must be string'})
    password: string;
}