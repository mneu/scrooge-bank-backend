import {
    Body,
    ConflictException,
    Controller,
    ForbiddenException,
    Get,
    Param,
    Post,
    Req,
    UseGuards
} from '@nestjs/common';
import {CustomerService} from "./customer.service";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";
import {UserActiveGuard} from "../auth/guards/userActiveGuard.guard";
import {CustomerOpenAccountDto} from "./dto/customerOpenAccount.dto";
import {UserTypeEnum} from "../user/enums/userTypeEnum";

@Controller('customer')
export class CustomerController {
    constructor(private readonly customerService: CustomerService) {
    }

    @Post('createAccount')
    async createAccount(@Req() request, @Body() customerOpenAccountDto: CustomerOpenAccountDto) {
        await this.customerService.createAccount(customerOpenAccountDto);
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Get('closeAccount/:customerId')
    async closeAccount(@Req() request, @Param('customerId') customerId: number) {
        let customerIdUserRequest = await this.customerService.getCustomerIdByUserId(request.jwtToken.sub);
        let canCloseAccount: boolean = false;

        if (request.jwtToken.roles.includes(UserTypeEnum.Customer) && customerId != customerIdUserRequest)
            throw new ForbiddenException({error: "You can't close an account of another customer"});
        else if (!request.jwtToken.roles.includes(UserTypeEnum.Customer)) // Is bank admin or bank user
            canCloseAccount = true;
        else //Is the same user
            canCloseAccount = true;

        if (canCloseAccount)
            await this.customerService.closeAccount(customerId);
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Get('accountBalance/:customerId')
    async accountBalance(@Req() request, @Param('customerId') customerId: number) {
        let customerIdUserRequest = await this.customerService.getCustomerIdByUserId(request.jwtToken.sub);
        let canRequest: boolean = false;

        if (request.jwtToken.roles.includes(UserTypeEnum.Customer) && customerId != customerIdUserRequest)
            throw new ForbiddenException({error: "You can't get balance of another customer"});
        else if (!request.jwtToken.roles.includes(UserTypeEnum.Customer)) // Is bank admin or bank user
            canRequest = true;
        else //Is the same user
            canRequest = true;

        if (canRequest)
            return await this.customerService.accountBalance(customerId);
        else
            return null;
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Get('loanStatus/:customerId')
    async loanStatus(@Req() request, @Param('customerId') customerId: number) {
        let customerIdUserRequest = await this.customerService.getCustomerIdByUserId(request.jwtToken.sub);
        let canRequest: boolean = false;

        if (request.jwtToken.roles.includes(UserTypeEnum.Customer) && customerId != customerIdUserRequest)
            throw new ForbiddenException({error: "You can't get loans details of another customer"});
        else if (!request.jwtToken.roles.includes(UserTypeEnum.Customer)) // Is bank admin or bank user
            canRequest = true;
        else //Is the same user
            canRequest = true;

        if (canRequest)
            return await this.customerService.loanStatus(customerId);
        else
            return null;
    }
}
