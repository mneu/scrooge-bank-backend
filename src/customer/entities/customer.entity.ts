import {
    Column,
    Entity,
    Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany, OneToOne,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";
import {Bank} from "../../bank/entities/bank.entity";
import {Loan} from "../../loan/entities/loan.entity";
import {User} from "../../user/entities/user.entity";
import {Operation} from "../../operation/entities/operation.entity";

@Entity({name: 'customers'})
export class Customer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    @Index()
    bankId: number;

    @Column({nullable: false})
    @Index()
    userId: number;

    @Column({length: 200, nullable: false})
    @Index()
    document: string; //IC, ID card, citizen card or passport card

    @Column({length: 1500, nullable: false})
    @Index()
    firstName: string;

    @Column({length: 1500, nullable: false})
    @Index()
    lastName: string;

    @Column({length: 1500, nullable: false})
    @Index() // Maybe an user try to find a customer by address
    address: string;

    @Column({length: 500, nullable: false})
    @Index() // Maybe an user try to find a customer by email
    email: string;

    @Column({type: 'boolean', nullable: false})
    mailConfirmed: boolean;

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    accountMoney: number;

    @Column({type: 'boolean', nullable: false})
    active: boolean; //False if account is closed

    @Column({type: "timestamp without time zone", nullable: false})
    createdAt: Date;

    @ManyToOne(type => Bank, bank => bank.customers)
    @JoinColumn({ name: 'bankId' })
    bank: Bank;

    @OneToMany(type => Loan, loan => loan.customerId)
    loans: Loan[];

    @OneToMany(type => Operation, operation => operation.customerId)
    operations: Operation[];

    @OneToOne(type => User, user => user.customerId)
    @JoinColumn({ name: 'userId' })
    user: User;
}