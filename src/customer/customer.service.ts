import {ConflictException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Customer} from "./entities/customer.entity";
import {getManager, Repository} from "typeorm";
import {User} from "../user/entities/user.entity";
import {UserTypeEnum} from "../user/enums/userTypeEnum";
import bcrypt = require("bcrypt");
import {CustomerOpenAccountDto} from "./dto/customerOpenAccount.dto";
import {Bank} from "../bank/entities/bank.entity";
import {Loan} from "../loan/entities/loan.entity";

@Injectable()
export class CustomerService {
    constructor(
        @InjectRepository(Customer) private customerRepository: Repository<Customer>,
        @InjectRepository(User) private userRepository: Repository<User>,
        @InjectRepository(Bank) private bankRepository: Repository<Bank>,
        @InjectRepository(Loan) private loanRepository: Repository<Loan>
    ) {
    }

    async getUserIdByCustomer(customerId: number): Promise<number> {
        let userPartial: any = await this.customerRepository
            .createQueryBuilder('c')
            .select(['c.userId as userid'])
            .where({'id': customerId})
            .getRawOne();

        if (!userPartial)
            return null;

        return userPartial.userid;
    }

    async getCustomerIdByUserId(userId: number): Promise<number> {
        let customerPartial: any = await this.customerRepository
            .createQueryBuilder('c')
            .select(['c.id as id'])
            .where({'userId': userId})
            .getRawOne();

        if (!customerPartial)
            return null;

        return customerPartial.id;
    }

    async createAccount(customerOpenAccountDto: CustomerOpenAccountDto) {
        let bankInDatabase: any = await this.bankRepository.findOne({id: customerOpenAccountDto.bankId});

        if (!bankInDatabase)
            throw new ConflictException({error: "Bank does not exists"});

        let customerInDatabase: any = await this.customerRepository.findOne({document: customerOpenAccountDto.document.trim().toLowerCase()});

        if (customerInDatabase)
            throw new ConflictException({error: "Customer already exists, you already have an account"});
        else {
            await getManager().transaction(async transactionalEntityManager => {
                let customerUser = new User();
                customerUser.firstName = customerOpenAccountDto.firstName.trim();
                customerUser.lastName = customerOpenAccountDto.lastName.trim();
                customerUser.email = customerOpenAccountDto.email.trim().toLowerCase();
                customerUser.password = await bcrypt.hash(customerOpenAccountDto.password, 10);
                customerUser.userTypeId = UserTypeEnum.Customer;
                customerUser.active = true;
                customerUser.mailConfirmed = true;
                customerUser.createdAt = new Date();
                customerUser.bankId = customerOpenAccountDto.bankId;
                await transactionalEntityManager.save(customerUser);

                let customer: Customer = new Customer();
                customer.firstName = customerOpenAccountDto.firstName.trim();
                customer.lastName = customerOpenAccountDto.lastName.trim();
                customer.document = customerOpenAccountDto.document.trim().toLowerCase();
                customer.address = customerOpenAccountDto.address.trim();
                customer.email = customerOpenAccountDto.email.trim().toLowerCase();
                customer.accountMoney = 0;
                customer.active = true; //Account is open
                customer.mailConfirmed = true;
                customer.createdAt = new Date();
                customer.bankId = customerOpenAccountDto.bankId;
                customer.userId = customerUser.id;
                await transactionalEntityManager.save(customer);

                //Update the user with the customerId
                customerUser.customerId = customer.id;
                await transactionalEntityManager.save(customerUser);
            });
        }
    }

    async closeAccount(customerId: number) {
        await getManager().transaction(async transactionalEntityManager => {
            let customer: Customer = await this.customerRepository.findOne({id: customerId});

            if (!customer.active)
                throw new ConflictException({error: "Account already closed"});

            if (customer.accountMoney > 0)
                throw new ConflictException({error: `You have $${customer.accountMoney} in your accunt, please witdraw before close your account`});

            if (customer.accountMoney < 0)
                throw new ConflictException({error: `You have pending debt for $${customer.accountMoney * -1}, please pay it before close your account`});

            let unpaidLoans: any = await this.loanRepository.count({customerId: customerId, finalized: false});

            if (unpaidLoans > 0)
                throw new ConflictException({error: "You have pending loans, please pay it before close your account"});

            //Disable user and customer (account closed)
            let user: any = await this.userRepository.findOne({id: customer.userId});
            customer.active = false;
            user.active = false;
            await transactionalEntityManager.save(customer);
            await transactionalEntityManager.save(user);
        });
    }

    async accountBalance(customerId: number) {
        let customer: Customer = await this.customerRepository.findOne({id: customerId});

        if (!customer)
            throw new ConflictException({error: "Customer does not exists"});

        if (!customer.active)
            throw new ConflictException({error: "Account already closed"});

        return {accountMoney: Number(customer.accountMoney)};
    }

    async loanStatus(customerId: number) {
        let customer: Customer = await this.customerRepository.findOne({id: customerId});

        if (!customer)
            throw new ConflictException({error: "Customer does not exists"});

        if (!customer.active)
            throw new ConflictException({error: "Account already closed"});

        let loans: Loan[] = await this.loanRepository.find({customerId: customerId});
        let result: any = [];

        loans.forEach((loan: Loan) => {
            result.push({
                amount: Number(loan.amount),
                paid: Number(loan.paid),
                finalized: loan.finalized,
                createdAt: loan.createdAt
            });
        });

        return result;
    }
}
