import {IsEmail, IsNotEmpty, IsNumber, IsString, MaxLength} from "class-validator";

export class LoanPaymentDto {
    @IsNotEmpty({message: 'You must define loanId'})
    @IsNumber({},{message: 'loanId must be a number'})
    loanId: number;

    @IsNotEmpty({message: 'You must define amount'})
    @IsNumber({},{message: 'amount must be a number'})
    amount: number;
}