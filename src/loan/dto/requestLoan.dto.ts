import {IsEmail, IsNotEmpty, IsNumber, IsString, MaxLength} from "class-validator";

export class RequestLoanDto {
    @IsNotEmpty({message: 'You must define customerId'})
    @IsNumber({},{message: 'customerId must be a number'})
    customerId: number;

    @IsNotEmpty({message: 'You must define amount'})
    @IsNumber({},{message: 'amount must be a number'})
    amount: number;
}