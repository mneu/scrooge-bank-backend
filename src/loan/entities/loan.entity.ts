import {
    Column,
    Entity,
    Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";
import {Bank} from "../../bank/entities/bank.entity";
import {Customer} from "../../customer/entities/customer.entity";
import {User} from "../../user/entities/user.entity";
import {LoanPayment} from "./loanPayment.entity";

@Entity({name: 'loans'})
export class Loan {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    @Index()
    customerId: number;

    @Column({nullable: false})
    @Index()
    createdByUserId: number; // Maybe created by customer user or maybe by bank admin user

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    amount: number;

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    paid: number;

    @Column({type: "boolean", nullable: false})
    @Index()
    finalized: boolean;
    
    @Column({type: "timestamp", nullable: false})
    createdAt: Date;

    @ManyToOne(type => Customer, customer => customer.loans)
    @JoinColumn({ name: 'customerId' })
    customer: Customer;

    @ManyToOne(type => User, user => user.loansCreated)
    @JoinColumn({ name: 'createdByUserId' })
    createdByUser: User;

    @OneToMany(type => LoanPayment, payment => payment.loanId)
    payments: LoanPayment[];
}