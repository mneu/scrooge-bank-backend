import {
    Column,
    Entity,
    Index, JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn
} from "typeorm";
import {Bank} from "../../bank/entities/bank.entity";
import {Loan} from "./loan.entity";

@Entity({name: 'loan_payments'})
export class LoanPayment {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false})
    @Index()
    loanId: number;

    @Column({nullable: false, type: "decimal", precision: 18, scale: 2})
    amount: number;

    @Column({type: "timestamp", nullable: false})
    createdAt: Date;

    @ManyToOne(type => Loan, loan => loan.payments)
    @JoinColumn({ name: 'loanId' })
    loan: Loan;
}