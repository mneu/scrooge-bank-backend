import { Test, TestingModule } from '@nestjs/testing';
import { LoanService } from './loan.service';
import {Bank} from "../bank/entities/bank.entity";
import {Customer} from "../customer/entities/customer.entity";
import {LoanHelper} from "./helpers/loan.helper";

describe('LoanService - Validate Loan', () => {
  let service: LoanHelper = new LoanHelper();

  it('Valid Loan', () => {
    let customer: Customer = new Customer();
    let bank: Bank = new Bank();
    let amount: number = 3000;
    let result: boolean;

    customer.accountMoney = 1000;
    customer.active = true;

    bank.customersMoney = 3000;
    bank.maxLoanMoney = 250000;
    bank.totalMoney = 253000;
    
    try {
      result = service.validateLoanRequest(customer, bank, amount)
    } catch (e) {
      result = false;
    }
    
    expect(result).toBe(true)
  });

  it('Invalid Loan', () => {
    let customer: Customer = new Customer();
    let bank: Bank = new Bank();
    let amount: number = 30000000;
    let result: boolean;

    customer.accountMoney = 1000;
    customer.active = true;

    bank.customersMoney = 3000;
    bank.maxLoanMoney = 250000;
    bank.totalMoney = 253000;

    try {
      result = service.validateLoanRequest(customer, bank, amount)
    } catch (e) {
      result = false;
    }
    
    expect(result).toBe(false)
  });

  it('Account Closed', () => {
    let customer: Customer = new Customer();
    let bank: Bank = new Bank();
    let amount: number = 3000;
    let result: boolean;

    customer.accountMoney = 1000;
    customer.active = false;

    bank.customersMoney = 3000;
    bank.maxLoanMoney = 250000;
    bank.totalMoney = 253000;

    try {
      result = service.validateLoanRequest(customer, bank, amount)
    } catch (e) {
      result = false;
    }
    
    expect(result).toBe(false)
  });

  it('Account does not exists', () => {
    let customer: Customer = null;
    let bank: Bank = new Bank();
    let amount: number = 3000;
    let result: boolean;

    bank.customersMoney = 3000;
    bank.maxLoanMoney = 250000;
    bank.totalMoney = 253000;

    try {
      result = service.validateLoanRequest(customer, bank, amount)
    } catch (e) {
      result = false;
    }

    expect(result).toBe(false)
  });
});
