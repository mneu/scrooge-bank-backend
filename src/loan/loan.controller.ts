import {
    Body,
    ConflictException,
    Controller,
    ForbiddenException,
    Get,
    Param,
    Post,
    Req,
    UseGuards
} from '@nestjs/common';
import {LoanService} from "./loan.service";
import {JwtAuthGuard} from "../auth/guards/jwt-auth.guard";
import {UserActiveGuard} from "../auth/guards/userActiveGuard.guard";
import {UserTypeEnum} from "../user/enums/userTypeEnum";
import {OperationDto} from "../operation/dto/operation.dto";
import {RequestLoanDto} from "./dto/requestLoan.dto";
import {CustomerService} from "../customer/customer.service";
import {LoanPaymentDto} from "./dto/loanPayment.dto";

@Controller('loan')
export class LoanController {
    constructor(private readonly loanService: LoanService, private readonly customerService: CustomerService) {
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Post('requestLoan')
    async requestLoan(@Req() request, @Body() requestLoanDto: RequestLoanDto) {
        let customerUserId = await this.customerService.getUserIdByCustomer(requestLoanDto.customerId);

        if (!customerUserId)
            throw new ConflictException({error: "customerId not found"});

        if (request.jwtToken.roles.includes(UserTypeEnum.Customer) && customerUserId != request.jwtToken.sub)
            throw new ForbiddenException({error: "As a customer you can't request a loan for another customerId"});

        return await this.loanService.requestLoan(requestLoanDto, request.jwtToken.sub);
    }

    @UseGuards(JwtAuthGuard)
    @UseGuards(UserActiveGuard)
    @Post('loanPayment')
    async loanPayment(@Req() request, @Body() loanPaymentDto: LoanPaymentDto) {
        return await this.loanService.loanPayment(loanPaymentDto);
    }
}
