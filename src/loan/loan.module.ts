import { Module } from '@nestjs/common';
import { LoanService } from './loan.service';
import { LoanController } from './loan.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../user/entities/user.entity";
import {Customer} from "../customer/entities/customer.entity";
import {Bank} from "../bank/entities/bank.entity";
import {Loan} from "./entities/loan.entity";
import {AuthModule} from "../auth/auth.module";
import {CustomerModule} from "../customer/customer.module";

@Module({
  imports: [TypeOrmModule.forFeature([User, Customer, Bank, Loan]), AuthModule, CustomerModule],
  providers: [LoanService],
  controllers: [LoanController]
})
export class LoanModule {}
