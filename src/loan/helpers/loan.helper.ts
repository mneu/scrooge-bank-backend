import {Customer} from "../../customer/entities/customer.entity";
import {Bank} from "../../bank/entities/bank.entity";
import {ConflictException} from "@nestjs/common";
import {Loan} from "../entities/loan.entity";

export class LoanHelper {
    validateLoanRequest(customer: Customer, bank: Bank, amount: number): boolean {
        if (!customer)
            throw new ConflictException({error: "Customer does not exists"});

        if (!customer.active)
            throw new ConflictException({error: "Account is closed"});

        if (Number(bank.customersMoney) * 0.25 < amount && Number(bank.maxLoanMoney) < amount)
            throw new ConflictException({error: "Mr. Scrooge is broken, we haven't money for your loan"});

        return true;
    }

    validateLoanPayment(loan: Loan, amount: number): boolean {
        if (!loan)
            throw new ConflictException({error: "Loan does not exists"});

        if (loan.finalized)
            throw new ConflictException({error: "The loan is totally paid"});

        if (loan.amount - loan.paid < amount)
            throw new ConflictException({error: "You can not pay more than you owe"});

        return true;
    }
}