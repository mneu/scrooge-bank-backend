import {ConflictException, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Customer} from "../customer/entities/customer.entity";
import {getManager, Repository} from "typeorm";
import {User} from "../user/entities/user.entity";
import {Bank} from "../bank/entities/bank.entity";
import {Loan} from "./entities/loan.entity";
import {RequestLoanDto} from "./dto/requestLoan.dto";
import {LoanPaymentDto} from "./dto/loanPayment.dto";
import {LoanPayment} from "./entities/loanPayment.entity";
import {LoanHelper} from "./helpers/loan.helper";

const loanHelper: LoanHelper = new LoanHelper();

@Injectable()
export class LoanService {
    constructor(
        @InjectRepository(Customer) private customerRepository: Repository<Customer>,
        @InjectRepository(User) private userRepository: Repository<User>,
        @InjectRepository(Bank) private bankRepository: Repository<Bank>,
        @InjectRepository(Loan) private loanRepository: Repository<Loan>
    ) {
    }
    
    async requestLoan(requestLoanDto: RequestLoanDto, logedUserId: number) {
        await getManager().transaction(async transactionalEntityManager => {
            let customer: Customer = await this.customerRepository.findOne({id: requestLoanDto.customerId});
            let bank: Bank = await this.bankRepository.findOne({id: customer.bankId});

            if (loanHelper.validateLoanRequest(customer, bank, requestLoanDto.amount)) {
                customer.accountMoney = Number(customer.accountMoney) + requestLoanDto.amount;
                bank.totalMoney = Number(bank.totalMoney) - requestLoanDto.amount;

                if (bank.maxLoanMoney <= requestLoanDto.amount) {
                    bank.maxLoanMoney = Number(bank.maxLoanMoney) - requestLoanDto.amount;
                }

                let loan: Loan = new Loan();
                loan.finalized = false;
                loan.customerId = requestLoanDto.customerId;
                loan.createdByUserId = logedUserId;
                loan.amount = requestLoanDto.amount;
                loan.createdAt = new Date();
                loan.paid = 0;

                await transactionalEntityManager.save(customer);
                await transactionalEntityManager.save(bank);
                await transactionalEntityManager.save(loan);
            }
        });
    }

    async loanPayment(loanPaymentDto: LoanPaymentDto) {
        await getManager().transaction(async transactionalEntityManager => {
            let loan: Loan = await this.loanRepository.findOne({id: loanPaymentDto.loanId});

            if (loanHelper.validateLoanPayment(loan, loanPaymentDto.amount)) {
                let customer: Customer = await this.customerRepository.findOne({id: loan.customerId});
                let bank: Bank = await this.bankRepository.findOne({id: customer.bankId});

                customer.accountMoney = Number(customer.accountMoney) - loanPaymentDto.amount;
                bank.totalMoney = Number(bank.totalMoney) + loanPaymentDto.amount;

                let loanPayment: LoanPayment = new LoanPayment();
                loanPayment.amount = loanPaymentDto.amount;
                loanPayment.createdAt = new Date();
                loanPayment.loanId = loanPaymentDto.loanId;

                loan.paid = Number(loan.paid) + loanPaymentDto.amount;
                loan.finalized = loan.amount - loan.paid == 0;

                await transactionalEntityManager.save(customer);
                await transactionalEntityManager.save(bank);
                await transactionalEntityManager.save(loanPayment);
                await transactionalEntityManager.save(loan);
            }
        });
    }
}
