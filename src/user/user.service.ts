import { Injectable } from '@nestjs/common';
import {Repository} from "typeorm";
import {User} from "./entities/user.entity";
import {InjectRepository} from "@nestjs/typeorm";

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) {
    }

    async findByUserName(email: string): Promise<User | undefined> {
        return this.userRepository.findOne({ 'email': email.toLowerCase() });
    }
}
