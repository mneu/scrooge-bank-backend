import {Column, Entity, ManyToOne, OneToMany, PrimaryColumn} from "typeorm";
import {User} from "./user.entity";

@Entity({name: 'user_types'})
export class UserType {
    constructor(id: number = 0, nombre: string = "") {
        this.id = id;
        this.name = nombre;
    }

    @PrimaryColumn('smallint')
    id: number;

    @Column({length: 300, nullable: false})
    name: string;

    @OneToMany(type => User, usuario => usuario.userTypeId)
    users: User[];
}