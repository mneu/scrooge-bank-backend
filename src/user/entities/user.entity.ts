import {
    Column,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn
} from "typeorm";
import {Bank} from "../../bank/entities/bank.entity";
import {UserType} from "./UserType.entity";
import {Customer} from "../../customer/entities/customer.entity";
import {Loan} from "../../loan/entities/loan.entity";
import {Operation} from "../../operation/entities/operation.entity";

@Entity({name: 'users'})
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true})
    @Index()
    bankId: number;

    @Column({nullable: true})
    @Index()
    customerId: number;

    @Column({type: 'smallint', nullable: false})
    @Index()
    userTypeId: number;

    @Column({length: 400, nullable: true})
    firstName: string;

    @Column({length: 400, nullable: true})
    lastName: string;

    @Column({length: 500, nullable: false})
    @Index() // Indexed because is the username, this speed up the login process
    email: string;

    @Column({length: 300, nullable: false})
    password: string;
    
    @Column({type: 'boolean', nullable: false})
    mailConfirmed: boolean;

    @Column({type: 'boolean', nullable: false})
    active: boolean;

    @Column({type: 'timestamp without time zone', nullable: false})
    createdAt: Date;

    @ManyToOne(type => Bank, bank => bank.users)
    @JoinColumn({ name: 'bankId' })
    bank: Bank;

    @ManyToOne(type => UserType, userType => userType.users)
    @JoinColumn({ name: 'userTypeId' })
    userType: UserType;

    @OneToOne(type => Customer, customer => customer.userId)
    @JoinColumn({ name: 'customerId' })
    customer: Customer;

    @OneToMany(type => Loan, loan => loan.createdByUserId)
    loansCreated: Loan[];

    @OneToMany(type => Operation, operation => operation.createdByUserId)
    operationsCreated: Operation[];
}