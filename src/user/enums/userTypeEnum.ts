export enum UserTypeEnum {
    BankAdmin = 1,
    BankUser = 2,
    Customer = 3
}