import { Injectable } from '@nestjs/common';
import {User} from "../user/entities/user.entity";
import {Bank} from "../bank/entities/bank.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Connection, Repository} from "typeorm";
import {UserType} from "../user/entities/UserType.entity";
import {UserTypeEnum} from "../user/enums/userTypeEnum";
import bcrypt = require("bcrypt");
import {Customer} from "../customer/entities/customer.entity";
import {OperationType} from "../operation/entities/operationType.entity";
import {OperationTypeEnum} from "../operation/enums/operationTypeEnum";

@Injectable()
export class DatabaseService {
    constructor(
        private connection: Connection,
        @InjectRepository(User) private usersRepository: Repository<User>,
        @InjectRepository(Bank) private tipoUsuarioRepository: Repository<Bank>,
    ) {
    }

    async seed() {
        console.info("Starting seed...");
        const queryRunner = this.connection.createQueryRunner();

        await this.connection.runMigrations({transaction: "all"});

        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            if (await queryRunner.manager.count(UserType) == 0) {
                await queryRunner.manager.save(new UserType(UserTypeEnum.BankAdmin, "Bank Admin"));
                await queryRunner.manager.save(new UserType(UserTypeEnum.BankUser, "Bank User"));
                await queryRunner.manager.save(new UserType(UserTypeEnum.Customer, "Customer"));
            }
            
            if (await queryRunner.manager.count(OperationType) == 0) {
                await queryRunner.manager.save(new OperationType(OperationTypeEnum.Deposit, "Deposit"));
                await queryRunner.manager.save(new OperationType(OperationTypeEnum.Withdraw, "Withdraw"));
            }
            
            let bankDemo: Bank;
            if (await queryRunner.manager.count(Bank) == 0) {
                bankDemo = new Bank();
                bankDemo.createdAt = new Date();
                bankDemo.name = "Scrooge Bank";
                bankDemo.maxLoanMoney = 250000;
                bankDemo.totalMoney = 250000;
                bankDemo.customersMoney = 0;
                bankDemo.address = "733 William S Canning Blvd, Fall River MA 2721"
                await queryRunner.manager.save(bankDemo);
            }

            let customerUser1: User;
            let customerUser2: User;
            if (await queryRunner.manager.count(User) == 0) {
                let adminUser: User = new User();
                adminUser.firstName = "Ebenezer";
                adminUser.lastName = "Scrooge";
                adminUser.email = "scrooge@gmail.com";
                adminUser.password = await bcrypt.hash("password123", 10);
                adminUser.userTypeId = UserTypeEnum.BankAdmin;
                adminUser.active = true;
                adminUser.mailConfirmed = true;
                adminUser.createdAt = new Date();
                adminUser.bankId = bankDemo.id;
                await queryRunner.manager.save(adminUser);

                customerUser1 = new User();
                customerUser1.firstName = "Bob";
                customerUser1.lastName = "Cratchit";
                customerUser1.email = "bob.cratchit@gmail.com";
                customerUser1.password = await bcrypt.hash("christmasghosts", 10);
                customerUser1.userTypeId = UserTypeEnum.Customer;
                customerUser1.active = true;
                customerUser1.mailConfirmed = true;
                customerUser1.createdAt = new Date();
                customerUser1.bankId = bankDemo.id;
                await queryRunner.manager.save(customerUser1);

                customerUser2 = new User();
                customerUser2.firstName = "Tim";
                customerUser2.lastName = "Tiny";
                customerUser2.email = "tiny.tim@gmail.com";
                customerUser2.password = await bcrypt.hash("christmasghosts", 10);
                customerUser2.userTypeId = UserTypeEnum.Customer;
                customerUser2.active = true;
                customerUser2.mailConfirmed = true;
                customerUser2.createdAt = new Date();
                customerUser2.bankId = bankDemo.id;
                await queryRunner.manager.save(customerUser2);
            }

            if (await queryRunner.manager.count(Customer) == 0) {
                let customer1: Customer = new Customer();
                customer1.firstName = "Bob";
                customer1.lastName = "Cratchit";
                customer1.document = "33775599";
                customer1.address = "993 Christmas Ghost Av., Fall River MA 2721";
                customer1.email = "bob.cratchit@gmail.com";
                customer1.accountMoney = 0;
                customer1.active = true; //Account is open
                customer1.mailConfirmed = true;
                customer1.createdAt = new Date();
                customer1.bankId = bankDemo.id;
                customer1.userId = customerUser1.id;
                await queryRunner.manager.save(customer1);
                //Update the user with the customerId
                customerUser1.customerId = customer1.id;
                await queryRunner.manager.save(customerUser1);

                let customer2: Customer = new Customer();
                customer2.firstName = "Tim";
                customer2.lastName = "Tiny";
                customer2.document = "44333562";
                customer2.address = "355 Christmas Ghost Av., Fall River MA 2721";
                customer2.email = "tiny.tim@gmail.com";
                customer2.accountMoney = 0;
                customer2.active = true; //Account is open
                customer2.mailConfirmed = true;
                customer2.createdAt = new Date();
                customer2.bankId = bankDemo.id;
                customer2.userId = customerUser2.id;
                await queryRunner.manager.save(customer2);
                //Update the user with the customerId
                customerUser2.customerId = customer2.id;
                await queryRunner.manager.save(customerUser2);
            }

            await queryRunner.commitTransaction();
        } catch (error) {
            await queryRunner.rollbackTransaction();
            console.error(error);
            console.error("Seed error!");
        }
    }
}
