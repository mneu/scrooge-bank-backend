import { Module } from '@nestjs/common';
import { DatabaseService } from './database.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import * as ormconfig from "../ormconfig";
import {Bank} from "../bank/entities/bank.entity";
import {Loan} from "../loan/entities/loan.entity";
import {Customer} from "../customer/entities/customer.entity";
import {BankModule} from "../bank/bank.module";
import {LoanModule} from "../loan/loan.module";
import {CustomerModule} from "../customer/customer.module";
import {UserModule} from "../user/user.module";
import {User} from "../user/entities/user.entity";

@Module({    imports: [
    TypeOrmModule.forRoot({
      ...ormconfig,
      keepConnectionAlive: true,
      autoLoadEntities: true
    }),
    TypeOrmModule.forFeature([Bank, Customer, Loan, User]),
    //TypeOrmModule.forFeature(),
    BankModule, CustomerModule, LoanModule, UserModule],
  providers: [DatabaseService]
})
export class DatabaseModule {}
