import {Injectable, ExecutionContext, Inject, forwardRef} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {AuthService} from '../auth.service';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    constructor(
        private authService: AuthService
    ) {
        super();
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        try {
            const request = context.switchToHttp().getRequest();
            const token = await this.authService.extractToken(request);
            const tokenDecoded = await this.authService.decodeToken(await this.authService.extractToken(request));
            let authOk = await this.authService.jwtIsValid(token);
            
            if (authOk)
                request.jwtToken = tokenDecoded;

            return authOk;
        } catch (e) {
            console.error(e);
            return false;
        }
    }
}