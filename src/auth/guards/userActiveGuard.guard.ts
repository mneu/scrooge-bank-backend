import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {AuthService} from "../auth.service";

@Injectable()
export class UserActiveGuard implements CanActivate {
    constructor(private reflector: Reflector, private authService: AuthService) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const tokenDecoded = await this.authService.decodeToken(await this.authService.extractToken(request));

        if (tokenDecoded == null)
            return false;

        return await this.authService.isUserActive(tokenDecoded.sub, tokenDecoded.roles);
    }
}