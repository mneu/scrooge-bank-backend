import {Injectable, CanActivate, ExecutionContext} from '@nestjs/common';
import {Reflector} from '@nestjs/core';
import {ROLES_KEY} from "../decorators/roles.decorator";
import {AuthService} from "../auth.service";
import {UserTypeEnum} from "../../user/enums/userTypeEnum";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector, private authService: AuthService) {
    }

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const requiredRoles = this.reflector.getAllAndOverride<UserTypeEnum[]>(ROLES_KEY, [
            context.getHandler(),
            context.getClass(),
        ]);
        if (!requiredRoles) {
            return true;
        }

        const request = context.switchToHttp().getRequest();
        const tokenDecoded = await this.authService.decodeToken(await this.authService.extractToken(request));

        if (tokenDecoded == null)
            return false;

        return requiredRoles.some((role) => tokenDecoded.roles?.includes(role));
    }
}