import {forwardRef, Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {AuthController} from './auth.controller';
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {LocalStrategy} from "./local.strategy";
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../user/entities/user.entity";
import {UserModule} from "../user/user.module";

@Module({
    imports: [
        PassportModule,
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: {expiresIn: process.env.LOGIN_EXPIRES},
        }),
        TypeOrmModule.forFeature([User]),
        forwardRef(() => UserModule)],
    exports: [AuthService],
    providers: [AuthService, LocalStrategy],
    controllers: [AuthController]
})
export class AuthModule {
}
