import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ExtractJwt } from "passport-jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { Connection, Repository } from "typeorm";
import {User} from "../user/entities/user.entity";
import {UserService} from "../user/user.service";

@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService,
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private connection: Connection,
    ) {
    }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.userService.findByUserName(username);

        if (user && user.active && await bcrypt.compare(pass, user.password)) {
            const { password, ...result } = user;

            return { ...result };
        }
        
        return null;
    }

    async jwtIsValid(token: string): Promise<boolean> {
        try {
            await this.jwtService.verifyAsync(token);
            return true;
        } catch (e) {
            return false;
        }
    }

    async extractToken(request: any): Promise<any> {
        if (request.query && request.query.jwt)
            return request.query.jwt;
        else
            return ExtractJwt.fromAuthHeaderAsBearerToken()(request);
    }

    async decodeToken(token: string): Promise<any> {
        return this.jwtService.decode(token);
    }

    async login(user: any) {
        const payload = {
            name: user.firstName + " " + user.lastName,
            sub: user.id,
            roles: [user.userTypeId]
        };
        return {
            access_token: await this.jwtService.signAsync(payload)
        };
    }

    async isUserActive(idUser: string, roles: number[]): Promise<boolean> {
        let resultUserActivo = await this.userRepository
            .createQueryBuilder('u')
            .select('u.active', 'active')
            .where({ 'id': idUser })
            .getRawOne();

        return resultUserActivo.active;
    }
}
