import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BankModule } from './bank/bank.module';
import { CustomerModule } from './customer/customer.module';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigModule} from "@nestjs/config";
import { LoanModule } from './loan/loan.module';
import { DatabaseModule } from './database/database.module';
import { UserModule } from './user/user.module';
import { OperationModule } from './operation/operation.module';
import { AuthModule } from './auth/auth.module';
import * as ormconfig from './ormconfig';
import {APP_GUARD} from "@nestjs/core";
import {RolesGuard} from "./auth/guards/roles.guard";

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      ...ormconfig,
      keepConnectionAlive: true,
      autoLoadEntities: true
    }),
    BankModule, CustomerModule, LoanModule, DatabaseModule, UserModule, OperationModule, AuthModule],
  controllers: [AppController],
  providers: [AppService,
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },],
})
export class AppModule {}
